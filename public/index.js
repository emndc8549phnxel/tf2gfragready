// @license magnet:?xt=urn:btih:723febf9f6185544f57f0660a41489c7d6b4931b&dn=wtfpl.txt WTFPL
const interval = 5 * 60 * 1000; // 5 mins in ms

//really dumb heartbeat, always runs no matter what
function heartbeat() {
    const req = new XMLHttpRequest();
    req.open('GET', '/heartbeat');
    // we generally don't care about the response
    // the headers will set the cookies and that's all that's needed
    // we'll log errors though
    req.onerror = function() {
        console.error("Unable to reach server. Either the server's down, or your internet is fucked.");
    };

    req.send();
}

setInterval(heartbeat, interval);
// @license-end
