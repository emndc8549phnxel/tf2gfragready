const path = require('path');
const flatCache = require('flat-cache');
const express = require('express');
const exphbs  = require('express-handlebars');
const nanoid = require('nanoid');
const cookieParser = require('cookie-parser');

const db = flatCache.load('ready.cache', path.resolve('./'));

const app = express();
app.use(cookieParser());
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

const port = process.env.PORT || 8008;
const invalidateAge = 60 * 6 * 1000; //6 min

function pruneDb() {
    Object.entries(db.all()).forEach(([key, val]) => {
        if (Date.now() - val > invalidateAge) {
            console.log(`Pruning ${key}`);
            db.removeKey(key);
        }
    });
    db.save(true);
}

function setInDb(key, val) {
    db.setKey(key, val);
    db.save(true);
}

function countInDb() {
    return db.keys().length;
}

function getInDb(key) {
    return db.getKey(key);
}

function hasInDb(key) {
    return db.getKey(key) !== undefined;
}

function removeFromDb(key) {
    db.removeKey(key);
    db.save(true);
}

function getIdIfValid(cookies) {
    let ret = {
        id: cookies.fragId,
        timestamp: cookies.fragTimestamp,
    }

    //if it doesn't exist, just return empty object
    if (ret.id === undefined || !hasInDb(ret.id)) {
        ret.timestamp = undefined;
        return ret;
    }

    //if it's expired, then it's invalid, too - delete in db btw
    if (Date.now() > ret.timestamp + invalidateAge) {
        removeFromDb(ret.id);
        ret.id = undefined;
        ret.timestamp = undefined;
        return ret;
    }

    //if not, refresh in DB, and set updated ID cookie in response
    ret.timestamp = Date.now();
    setInDb(ret.id, ret.timestamp);
    return ret;
}

app.set('views', './views');
app.use(express.static('./public'));

app.use(express.json());
app.use(express.urlencoded({ extended: false} ));

app.get('/', (req, res) => {
    console.log('Rendered main page!');
    //prune DB
    pruneDb();

    //check if ready and enable/disable buttons accordingly
    const { id, timestamp } = getIdIfValid(req.cookies);
    const enabled = {
        ready: true,
        unready: false,
    };
    if (id !== undefined) {
        enabled.ready = false;
        enabled.unready = true;
        // also refresh cookie
        const opt = {
            maxAge: invalidateAge,
        };
        res
            .cookie('fragId', id, opt)
            .cookie('fragTimestamp', timestamp, opt);
    }

    res.render('index', { readiesCount: countInDb, enabled, layout: false });
});

app.post('/readyup', (req, res) => {
    console.log('readied up');
    //set up cookie data
    let id = req.cookies.fragId;
    if (id === undefined) id = nanoid();
    let timestamp = Date.now();

    //update DB
    setInDb(id, timestamp);

    //set updated ID cookie and serve page
    const opts = {
        maxAge: invalidateAge,
        path: '/',
    };
    res
        .cookie('fragId', id, opts)
        .cookie('fragTimestamp', timestamp, opts)
        .redirect('/');
});

app.post('/unready', (req, res) => {
    console.log('unreadied');
    //get ID cookie
    let id = req.cookies.fragId;
    if (id !== undefined) {
        removeFromDb(id);
    }

    //delete ID cookie in response request
    const opts = {
        maxAge: 0,
    };
    res
        .cookie('fragId', 0, opts)
        .cookie('fragTimestamp', 0, opts)
        .redirect('/');
});

app.get('/heartbeat', (req, res) => {
    console.log('heartbeat');
    let maxAge = invalidateAge;

    let { id, timestamp } = getIdIfValid(req.cookies);
    if (id === undefined) { //didn't exist or timed out
        maxAge = 0;
    }

    //send updated cookie values
    res
        .cookie('fragId', id, { maxAge })
        .cookie('fragTimestamp', timestamp, { maxAge })
        .status(200).end();
});

app.listen(port, () => console.log(`Started on port ${port}`));
